// set the donor icon here
const DONORICON = 'cash-coin'

const fs = require('fs').promises
const YAML = require('yaml')


// read the markdown files into an array
const getFileContent = () => new Promise((resolve, reject) => {
  const dir = require('node-dir')

  let sections = []
  
  dir.readFiles('sections',
   function(err, content, next) {
     if (err) throw err;
     let title = content.match(/:(.*?)\n/)
     sections.push({
       'title': title[1].trim(),
       'content': content.substring(content.indexOf('\n') + 2)
     })

     next();
   },
   function(err, files){
     if (err) throw err;
     // console.log('finished reading files:', files); // get filepath 
     resolve(sections)
   }
  );
})

const donorIconSpan = async () => {
  // load donor icon buffer
  const donorIconBuf = await fs.readFile(`./public/donoricons/${DONORICON}.svg`)

  return `<span class="donoricon">${donorIconBuf.toString()}</span>`
} 

const getContent = async () => {
  const _donorIconSpan = await donorIconSpan()

  // get md of sections
  const getMd = require('./md2html.js')
  const mdArray = await getFileContent()

  // convert md to html
  const htmlArray = []
  for (const obj of mdArray) {
    const processedContent = await getMd(obj.content);
    
    htmlArray.push({
      ...obj,
      content: processedContent.replaceAll(/\$D\$/g, _donorIconSpan)
    });
  }

  return htmlArray
}

// stroke triggers
const getStrokeTriggers = async () => 
YAML.parse((await fs.readFile("strokeTriggers.yaml")).toString()).strokeTriggers

// main function. returns an html page
const main = async () => {
  // load pug
  const pug = require('pug')

  // get the page content from markdown files as html
  const content  = await getContent()

  // function to compile pug
  const compiledFunction = pug.compileFile('index.pug')

  // get donor icon span with svg
  const _donorIconSpan = await donorIconSpan()

  // compile pug
  return compiledFunction({
    'sections': content,
    'strokeTriggers': await getStrokeTriggers(),
    donorIcon: _donorIconSpan
  })
}

// build
(async () => {
  if (process.argv[2] && process.argv[2] === 'build') {
    const pageHtml = await main()
    await fs.writeFile('./public/index.html', pageHtml)
    console.log('build successful!')
    process.exit()  
  }
})();

// express stuffs
const express = require('express')
const app = express()
const port = 3000
app.use(express.static('public', {index: '_'}))

app.get('/', async (req, res) => {
  const pageHtml = await main()
  res.send(pageHtml)
})

app.listen(port, () => {
  console.log(`Dev server listening on port ${port}`)
})
