# ORANGE: Unwanted

Reject ALL orange topics.
These rules are not as serious as red topics, and many rules haven't been
provided to ordinary users.
Consider advising the user to avoid it before moderating them. 
Use moderator action if the user ignores warnings and repetively submits these.
For $D$, you can be more relaxed but still use discresion.

1. $D$ N-word and N-word baiting.
2. $D$ Instructions for illegal activities that are not public information.
3. $D$ Encouraging terrorism without clear satirical context.
4. $D$ Encouraging hate speech without clear satirical context.
5. $D$ Encouraging the audience to commit suicide.
6. $D$ Random gibberish (eg. cGFja2FnZSBtYWluCgpwcmludCBhbGxvdz0iUGF1c2UgZm9yIDMgbWludXRlczogMTgwIHNly29uZHNgKzE4MCBzZWNvbmRzIgp3aGlsZSB0aGlzIGlzUg).
7. Topics skipped by the stream shouldn't be re-submitted verbatim. They may be re-submitted by changing the wording.
8. Anything Loud House related.
