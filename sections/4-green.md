# GREEN: Allowed

These things aren't a reason to reject a topic by itself.
Remember you should still reject topics for being shit and unfunny, or if you
are uncertain.
Just because a topic has something in here, doesn't mean you should accept it.

1. Emojis.
2. Things you personally don't like or find annoying (eg. Chris screams
   "AAAAAAAAAAAA", Brian says "\*").
3. Tutorials on explosives such as nukes are fine because it's public
   information. **NOT** homemade explosives and weapons
   (eg. IED, pipe bomb, etc.)
4. Instructions on how to make meth.
5. Clearly fake personal information (eg. fake addresses, fake phone numbers,
   internal IPs)
   - internal IP addresses are in the following ranges:
     - 10.0.0.0 – 10.255.255.255
     - 172.16.0.0 – 172.31.255.255
     - 192.168.0.0 – 192.168.255.255
6. Sexualizing Stewie is not considered pedophilia.
   - This may still be rejected as a shit topic.
7. Sexualizing Brian is not considered zoophilia.
   - Brian fucking squirrels is not considered zoophilia.
8. Words that sound like the N-word but are not the hard-R are allowed (eg. Manager, Nugger, Knickers, Nick Gurr)
