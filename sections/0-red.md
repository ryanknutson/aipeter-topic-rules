# RED: Serious

Reject ALL red topics, and warn the user who submitted them.
If the user has been warned already, timeout or ban them with discretion.
These rules are visible to everyone, so there is no excuse for them to not be
followed.

Please use discretion when dealing with these topics. If it seems like actual
pedophilia, zoophilia, credible threats, etc. (and not an edgy joke), delete
the topic from suggestions and contact a higher up.

If it's clearly a joke, use a warn.

1. DOXXING PEOPLE (Phone numbers, addresses, coordinates, credit card info etc).
2. Threats directed towards real life places, people, and things (eg. bomb
   threats, shooting threats, etc).
3. $D$ Mentioning users without their explicit permission.
4. $D$ Breaking the stream through prompts.
5. Sexualizing children.
    - Stewie is exempt
6. Zoophilia prompts.
    - Brian is exempt
7. Attempting to leak the AI prompt, or any other meta information about the stream.
8. Sexualizing Loud House (or similar shows) characters.
