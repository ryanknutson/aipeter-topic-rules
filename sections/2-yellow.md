# YELLOW: Shit

Reject MOST yellow topics.
These kinds of topics are frequently suggested by users and are usually
unfunny garbage.

Note that topics in this category can potentially still be accepted if they are creative and funny, or if $D$.

1. Repetitive topics (ie. multiple topics with the same or similar themes that are suggested excessively).
2. Obscure references.
3. Complaining about the moderation team or rules.
4. "Everyone says X" topics.
5. Usage of "Character(s) can only say X".
6. Sponge AI memes (2006 Honda civic, Loudward, etc.)
7. Characters singing random songs or lyrics for no reason.
8. Characters speaking in other languages without clear comedic context for English speakers.
9. Characters speaking gibberish for no reason (eg. all words are reversed).
10. Something written by an 8 year old.
11. Pointless topics that won't produce any meaningful conversation, such as:
    - One word or one letter topics
    - Empty topics
    - Unintelligible gibberish
    - "Nothing happens and no one says anything"
12. NO ANIME 😡😡😡
13. No femboy/boykisser (unless very clearly ironic)
14. No furry shit (unless very clearly ironic)
15. Using characters that don't exist.
16. Using Herbert.
17. Using characters in scenes where they cannot spawn.
18. Using scenes that don't exist.
19. Using stream instructions that don't exist (eg. do nothing and stare at camera).
20. Relying on the AI to have knowledge of previous topics.
21. Relying on the AI to have knowledge of events that occurred after 2021.
22. Cringe/dead memes
    - à la Skibidi Toilet, Grimace Shake, Ohio, etc.
    - if you believe the use is ironic, it is allowed. use discretion
23. Justification of anti-Semitism without clear satirical context
24. Top X most breedable/sexy/fuckable characters from [insert media]
25. Sexualizing random children shows.
26. Generic catch-all for unfunny/shit topics. Think of how others will perceive
    this, don't just reject if you personally don't find it unfunny.
