# BLUE: Boring

CONSIDER rejecting blue topics **unless $D$** or very creative.
These are widely subjective guidelines. Their purpose is to avoid shit or
otherwise unfunny topics.


1. Long lists of boring concepts/ideas. For Example:
    - "(Carter only) Carter lists the top 10 scientific laws and theories in great detail"
    - "(Mort only) Mort says the name of every mountain on Earth in only a single breath"
2. Boring characters talking about boring things in a boring manner. For example:
    - "Everyone has a calm and detailed discussion about economic policies"
    - "(Brian only) Brian gives his top 10 photo editing techniques"
    - "(Stewie only) Stewie explains quantum mechanics to the audience"
    - "(Peter only) Peter explains the plot of Rick and Morty"
