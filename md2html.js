const markdownit = require('markdown-it')()
markdownit.use(require('markdown-it-multimd-table'),{
  multiline:  true,
  rowspan:    true,
  headerless: true
})
markdownit.use(require('markdown-it-attrs'))
markdownit.use(require('markdown-it-div'))


const createDOMPurify = require('dompurify');
const { JSDOM } = require('jsdom');

const getMd = async (content, emoji = true, purify = true) => {
  try {
    // enable emoji
    markdownit.use(require('markdown-it-emoji'))
    const twemoji = require('twemoji')
    markdownit.renderer.rules.emoji = function(token, idx) {
        return twemoji.parse(token[idx].content);
    };

    if (content === null) return null

    const window = new JSDOM('').window;
    const DOMPurify = createDOMPurify(window);

    // convert render the markdown into html
    const rawhtml = markdownit.render(content)

    // purify and return
    // if (purify) return DOMPurify.sanitize(rawhtml)
    // else return rawhtml
    return purify?DOMPurify.sanitize(rawhtml):rawhtml
  } catch(err) {
    console.error(err)
    throw new Error('failed to load markdown!')
  }
}

module.exports = getMd
